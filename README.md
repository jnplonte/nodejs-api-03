# NODE API

[![license](https://img.shields.io/github/license/mashape/apistatus.svg)]()

## Version

**v1.0.0**

## Dependencies

- nodejs: [https://nodejs.org/](https://nodejs.org/en/)
- expressjs: [https://expressjs.com/](https://expressjs.com/)
- docker: [https://www.docker.com/](https://www.docker.com/)

## Demo

- api: [http://nodeapi.jnpl.me/](http://nodeapi.jnpl.me/)
- api documentation: [http://nodeapi.jnpl.me/documentation/](http://nodeapi.jnpl.me/documentation/)

## NODE

### Installation

- Install typescript globally `npm install -g typescript`
- Install npm dependencies by running `npm install`
- Build typescript by running `npm run build:development`
- Get global config by running `npm run generate:config`
- Generate documentation by running `npm run generate:docs`

### How to Use

- run `npm start` it will listen to http://localhost:8383

### Testing

- start all test by running `npm run test`
- start typescript linter `npm run lint`

## DOCKER

### Installation

- build `docker-compose build`
- install node `docker exec -ti node npm install`
- install node `docker exec -ti node npm run generate:config`
- install node `docker exec -ti node npm run generate:docs`

### How to Use

- run `docker-compose up`
- run `docker-compose start`

### TEST STEP

- `git clone git@bitbucket.org:jnplonte/nodejs-api-03.git`
- `cp nodejs-api-03/node-app/app.env.example nodejs-api-03/node-app/app.env`
- `docker-compose build`
- `docker-compose up`
- `curl --location --request POST 'http://localhost:8383/v1/core/chat' --header 'x-node-api-key:b2ZpdWpldzg5cjg5dWo0cmV3dTBmdXNlOTBmZXc5ZjB3ZWlmOWl3ZWY5MGk=' --header 'Content-Type: application/json' --data-raw '{"conversationId": "ABC123", "message": "hello superman"}'`
