import * as apiConfig from './api-config.json';

export const baseConfig = {
	name: 'nodeapi',
	logo: 'https://via.placeholder.com/50',
	poweredBy: 'node api',
	secretKey: 'x-node-api-key',
	secretKeyHash: 'ofiujew89r89uj4rewu0fuse90few9f0weif9iwef90i',

	defaultAnswer: 'Sorry, I don’t understand.',

	api: apiConfig,
};
