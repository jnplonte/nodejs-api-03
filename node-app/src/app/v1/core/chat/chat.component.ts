import { Request, Response } from 'express';
import { CoreMiddleware } from '../../../middlewares/core/core.middleware';

import { IModels } from '../../../../interfaces/core.interface';

export class Chat extends CoreMiddleware {
	constructor(app, private nlpManager, private response, private helper) {
		super(app);

		this.nlpManager.load();
	}

	get services() {
		return {
			'POST /chat': 'post',
		};
	}

	/**
	 * @api {post} /core/chat chat
	 * @apiVersion 1.0.0
	 * @apiName post
	 * @apiGroup CHAT
	 * @apiPermission all
	 *
	 * @apiDescription chat
	 *
	 * @apiParam (body) {String} message chat message
	 * @apiParam (body) {String} conversationId conversation id
	 */
	post(req: Request, res: Response): void {
		const reqParameters: string[] = ['message', 'conversationId'];
		if (!this.helper.validateData(req.body, reqParameters)) {
			return this.response.failed(res, 'data', reqParameters);
		}

		return this.nlpManager
			.process('en', req.body.message)
			.then((message: IModels) => {
				const finalMessage: string = this.helper.isNotEmpty(message.answer)
					? message.answer
					: this.helper.defaultAnswer;
				this.response.success(res, 'chat', {
					responseId: req.body.conversationId as string,
					response: finalMessage,
				});
			})
			.catch((error) =>
				this.response.failed(res, 'chat', {
					responseId: req.body.conversationId as string,
					response: this.helper.defaultAnswer,
				})
			);
	}
}
