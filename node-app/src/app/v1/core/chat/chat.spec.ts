import * as supertest from 'supertest';
import { expect } from 'chai';

import app from '../../../../app';

const key: string = 'b2ZpdWpldzg5cjg5dWo0cmV3dTBmdXNlOTBmZXc5ZjB3ZWlmOWl3ZWY5MGk=';
describe('chat component', () => {
	it('hello from test', (done) => {
		supertest(app)
			.post('/v1/core/chat?test=true')
			.set('x-node-api-key', key)
			.send({
				conversationId: 'ABC123',
				message: 'hi',
			})
			.expect('Content-Type', /json/)
			.expect(200, (err, res) => {
				if (err) {
					return done(err);
				}

				expect(res.body.status).to.equal('success');
				expect(res.body.data.response).to.equal('Welcome to StationFive.');

				done();
			});
	});

	it('goodbye from test', (done) => {
		supertest(app)
			.post('/v1/core/chat?test=true')
			.set('x-node-api-key', key)
			.send({
				conversationId: 'ABC123',
				message: 'goodbye',
			})
			.expect('Content-Type', /json/)
			.expect(200, (err, res) => {
				if (err) {
					return done(err);
				}

				expect(res.body.status).to.equal('success');
				expect(res.body.data.response).to.equal('Thank you, see you around.');

				done();
			});
	});

	it('should not uderstand', (done) => {
		supertest(app)
			.post('/v1/core/chat?test=true')
			.set('x-node-api-key', key)
			.send({
				conversationId: 'ABC123',
				message: 'super',
			})
			.expect('Content-Type', /json/)
			.expect(200, (err, res) => {
				if (err) {
					return done(err);
				}

				expect(res.body.status).to.equal('success');
				expect(res.body.data.response).to.equal('Sorry, I don’t understand.');

				done();
			});
	});
});
