import { toJson, toString, isEmpty, isNotEmpty, checkObjectInList } from 'jnpl-helper';

export class Helper {
	env: string = process.env.NODE_ENV || 'local';

	constructor(private config) {}

	toJson(jsonData: any = ''): any {
		return toJson(jsonData);
	}

	toString(jsonData: any = ''): any {
		return toString(jsonData);
	}

	isNotEmpty(v: any = null): boolean {
		return isNotEmpty(v);
	}

	isEmpty(v: any = null): boolean {
		return isEmpty(v);
	}

	validateData(obj: Object = {}, list: Array<any> = []): boolean {
		return checkObjectInList(obj, list);
	}

	get secretKey(): string {
		return this.config.secretKey || '';
	}

	get secretHash(): string {
		return this.config.secretKeyHash || '';
	}

	get defaultAnswer(): string {
		return this.config.defaultAnswer || '';
	}
}
