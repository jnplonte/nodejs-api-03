import { expect } from 'chai';
import { Helper } from './helper.service';
import { baseConfig } from './../../../config';

describe('helper service', () => {
	let services;

	beforeEach((done) => {
		services = new Helper(baseConfig);
		done();
	});

	it('should get the expected json data', (done) => {
		expect(services.toJson('{"test": "test-data"}')).to.eql({ test: 'test-data' });

		done();
	});

	it('should get the expected string data', (done) => {
		expect(services.toString({ test: 'test-data' })).to.equal('{"test":"test-data"}');

		done();
	});

	it('should check if data is empty or not empty', (done) => {
		expect(services.isEmpty('')).to.be.true;

		expect(services.isNotEmpty('x')).to.be.true;

		done();
	});

	it('should check if data on array exists', (done) => {
		expect(services.validateData({ test: 'test-data' }, ['test'])).to.be.true;

		done();
	});

	it('should get default configs', (done) => {
		expect(services.secretKey).to.be.a('string');
		expect(services.secretHash).to.be.a('string');
		expect(services.defaultAnswer).to.be.a('string');

		done();
	});
});
